import { useEffect, useState } from 'react'
import { useAPI } from '../util/useAPI';
import { Offer } from './Offer';

/**
 * Creates the offer list to be displayed.
 * @param {object} location Holds the information of the router.
 * @param {object} history Holds information of the router.
 * @returns Offer list component.
 */
export const OffersList = ({ location, history }) => {
  // User id and location could be stored in context/redux on login.
  const { status, data, error } = useAPI(
    'get/offers?user_id=25879&location=Geelong',
  );
  const [offers, setOffers] = useState([])
  const [currentOffer, setCurrentOffer] = useState(null)

  /**
   * On status change, run to see if status is fetched (api call successfull) and set offers to the returned data.
   */
  useEffect(() => {
    if (status === 'fetched') {
      setOffers(data);
    }
  }, [status]);

  /**
   * Handles loading a offer page if in the url.
   */
  useEffect(() => {
    if (offers.length !== 0) {
      let result = offers.filter(offer => {
        return offer.slug === location.search.slice(1)
      })
      setCurrentOffer(result[0])
    }
  }, [offers])

  /**
   * On offer click, set the current offer to the offer clicked and set the url.
   * @param {object} offer The offer object being clicked.
   */
  const handleClick = (offer) => {
    setCurrentOffer(offer)
    history.push(`/offers?${offer.slug}`);
  }

  /**
   * Renders the offer menu, either loading, error or the data in a list.
   * @returns loading, error or data depending on the status of the api call.
   */
  const OfferMenu = () => {
    return (
      <div className='content'>
        {status === 'fetching' && (
          <div className="spinner" />)}
        {status === 'error' && (
          <h1>{error}</h1>
        )}
        {status === 'fetched' && offers.length !== 0 && (
          <>
            {offers.map((offer, index) => (
              <div key={index} onClick={() => handleClick(offer)}>
                <img key={index} src={offer.image} alt={offer.title} />
              </div>
            ))}
          </>)}
      </div>)
  }

  return currentOffer === null || location.search === ''
    ? <OfferMenu />
    : <Offer image={currentOffer.image} title={currentOffer.title} limited={currentOffer.limited} locations={currentOffer.locations} />

}
