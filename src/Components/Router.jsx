import React from 'react'
import { Switch, Route } from "react-router-dom";
import { OffersList } from './OffersList';
import { Empty } from './Empty';

/**
 * 
 * @returns 
 */
export const Router = () => {
  return (
    <Switch>
      <Route path="/home" component={Empty} />
      <Route path="/offers" component={OffersList} />
      <Route path="/tipping" component={Empty} />
      <Route path="/book" component={Empty} />
      <Route path="/more" component={Empty} />
    </Switch>
  )
}
