import React from 'react'
/**
 * Used for mocking the other tabs.
 * @returns An empty component.
 */
export const Empty = () => {
  return (
    <div>
      <h1>WIP</h1>
    </div>
  )
}
