import React from 'react'
import barcode from '../img/barcode.png'

/**
 * Handles rendering the offer information page.
 * @param {string} image The image to be displayed in the header.
 * @param {string} title Title of the offer.
 * @param {boolean} limited If the offer is a limited time (true) or not (false).
 * @param {string} locations A list of locations this offer is valid at.
 * @returns 
 */
export const Offer = ({image, title, limited, locations}) => {
  return (
    <div className='offerPage'>
      <img src={image} alt={title} />
      <h5>
        {title}
      </h5>
      <h5>Offer Sub Heading</h5>
      <p>
        Offer description, Offer description, Offer description, Offer
        description, Offer description, Offer description, Offer description
      </p>
      <h5>Offer Sub Heading</h5>
      <p>
        Offer description, Offer description, Offer description, Offer
        description, Offer description, Offer description, Offer description
      </p>
      {limited === true && (
        <h5>
          LIMITED TIME ONLY
        </h5>
      )}
      <h5 >VALID AT</h5>
      <p>
        {locations}
      </p>
      <h5>
        Scan the barcode to redeem
      </h5>
      <img style={{backgroundColor: 'white'}} src={barcode} alt=""/>
    </div>
  )
}
