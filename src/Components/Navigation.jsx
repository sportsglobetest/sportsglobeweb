import React, { useState } from 'react';
import { Nav, NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { mdiShieldOutline, mdiOffer, mdiCheckboxMarkedOutline, mdiSilverwareForkKnife, mdiMenu, mdiArrowLeft } from '@mdi/js';
import Icon from '@mdi/react'
import { useLocation, useHistory, } from 'react-router-dom'
import { withRouter } from "react-router-dom";

/**
 * Holds the information for populating the tab menu.
 */
const tabs = [{
  route: "/home",
  icon: mdiShieldOutline,
  label: "Home"
}, {
  route: "/offers",
  icon: mdiOffer,
  label: "Offers"
}, {
  route: "/tipping",
  icon: mdiCheckboxMarkedOutline,
  label: "Tipping"
}, {
  route: "/book",
  icon: mdiSilverwareForkKnife,
  label: "Book"
}, {
  route: "/more",
  icon: mdiMenu,
  label: "More"
}]

/**
 * Handles the creation of both the header and footer.
 * @returns The header and footer.
 */
const Navigation = () => {
  const history = useHistory();
  const location = useLocation();
  const [title, setTitle] = useState(location.pathname.slice(1))

  /**
   * Handles the creation of a back button to be used on offer information pages.
   * @returns A back button component.
   */
  const BackButton = () => <Icon className='backArrow' onClick={() => history.goBack()} path={mdiArrowLeft} size={1.5} />

  return (
    <div>
      <nav className="navbar navbar-expand-md navbar-light header fixed-top " role="navigation" style={{ alignItems: 'center' }}>
        {location.search !== "" &&
          <BackButton />
        }
        <h1>{title}</h1>
      </nav>
      <nav className="navbar fixed-bottom d-block bottom-tab-nav footer" role="navigation">
        <Nav className="w-100">
          <div className=" d-flex flex-row justify-content-around w-100">
            {
              tabs.map((tab, index) => (
                <NavItem key={`tab-${index}`} onClick={() => setTitle(tab.label)}>
                  <NavLink to={tab.route} className="nav-link bottom-nav-link" activeClassName="active">
                    <div className="row d-flex flex-column justify-content-center align-items-center">
                      <Icon style={{ flexGrow: 1 }} path={tab.icon}
                        title={tab.label}
                        size={1.5} />
                      <div className="bottom-tab-label">{tab.label}</div>
                    </div>
                  </NavLink>
                </NavItem>
              ))
            }
          </div>
        </Nav>
      </nav>
    </div>
  )
};

export default withRouter(Navigation);