import './App.scss';
import { BrowserRouter } from "react-router-dom";
import Navigation from './Components/Navigation'
import { Router } from './Components/Router';

/**
 * The entry point of the application.
 * @returns The application.
 */
function App() {

  return (
    <div className="App">
      <BrowserRouter>
        <Navigation />
        <div className="mainContent">
          <Router />
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;